package main

import (
	"crypto/tls"
	"errors"
	"html/template"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"strings"

	gomail "gopkg.in/gomail.v2"

	"github.com/gorilla/mux"
)

var (
	errNoCategories = errors.New("there are no categories selected")
)

// Data is the data sent to the homepage
type Data struct {
	Categories      []string
	Users           []string
	EmailList       []string
	CurrentCategory string
}

// Category represents a category with a name
type Category struct {
	ID   int
	Name string
}

// Email represents that belongs to a category and has an address
type Email struct {
	ID         int
	CategoryID int
	Address    string
}

// User represents a user registered in smtp.antoniopan.com.address
type User struct {
	Username string
	Password string
	Address  string
}

var templates = template.Must(template.ParseFiles("templates/mailer.html"))

func homepageHandler(w http.ResponseWriter, r *http.Request) {
	data := &Data{
		Categories:      []string{},
		Users:           []string{},
		EmailList:       []string{},
		CurrentCategory: "",
	}

	rows, err := db.Query("SELECT * FROM categories")
	if err != nil {
		log.Printf("failed to query categories: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	var cat Category
	for rows.Next() {
		rows.Scan(&cat.ID, &cat.Name)
		data.Categories = append(data.Categories, cat.Name)
	}
	defer rows.Close()

	rows, err = db.Query("SELECT address FROM users")
	if err != nil {
		log.Printf("failed to query users: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	var address string
	for rows.Next() {
		rows.Scan(&address)
		// send only address for now
		data.Users = append(data.Users, address)
	}
	vars := mux.Vars(r)
	category := vars["category"]
	row := db.QueryRow("SELECT id FROM categories WHERE name=$1", category)
	if err := row.Scan(&cat.ID); err == nil {
		rows, err := db.Query("SELECT address FROM emails WHERE category_id=$1", cat.ID)
		if err != nil {
			log.Printf("failed querying emails table: %v\n", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		var email Email
		for rows.Next() {
			rows.Scan(&email.Address)
			data.EmailList = append(data.EmailList, email.Address)
		}
	}

	err = templates.ExecuteTemplate(w, "mailer.html", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func newEmailHandler(w http.ResponseWriter, r *http.Request) {
	var catID int
	catName := r.PostFormValue("category")
	emailAddress := r.PostFormValue("email")

	row := db.QueryRow("SELECT id FROM categories WHERE name=$1", catName)
	if err := row.Scan(&catID); err != nil {
		log.Printf("category does not exist: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if _, err := db.Exec("INSERT INTO emails (address, category_id) VALUES ($1,$2)", emailAddress, catID); err != nil {
		log.Printf("failed to add email: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", "/emails/"+catName)
	w.WriteHeader(http.StatusFound)
}

func newCategoryHandler(w http.ResponseWriter, r *http.Request) {
	catName := r.PostFormValue("name")
	if catName == "" {
		log.Println("category can't be empty")
		http.Error(w, "category can't be empty", http.StatusInternalServerError)
		return
	}

	if _, err := db.Exec("INSERT INTO categories (name) VALUES ($1)", catName); err != nil {
		log.Printf("failed to insert category: %v\n", catName)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", "/")
	w.WriteHeader(http.StatusFound)
}

func newUserHandler(w http.ResponseWriter, r *http.Request) {
	username, password, email := r.PostFormValue("username"),
		r.PostFormValue("password"),
		r.PostFormValue("email")

	if username == "" || password == "" || email == "" {
		log.Println("username password or email should not be empty")
		http.Error(w, "username password or email should not be empty", http.StatusInternalServerError)
		return
	}

	if _, err := db.Exec("INSERT INTO users (username, password, address) VALUES ($1,$2,$3)", username, password, email); err != nil {
		log.Printf("failed to insert user: %v\n", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", "/")
	w.WriteHeader(http.StatusFound)
}

func mailerHandler(w http.ResponseWriter, r *http.Request) {
	subject := r.FormValue("subject")
	if subject == "" {
		subject = "Antonio Pan & Hijos"
	}

	authUser := r.FormValue("authuser")
	if authUser == "" {
		http.Error(w, "please provide the authenticated user", http.StatusBadRequest)
		w.Header().Set("Location", "/")
		w.WriteHeader(http.StatusTemporaryRedirect)
		return
	}
	row := db.QueryRow("SELECT username,password,address FROM users WHERE address=$1", authUser)
	var user User
	if err := row.Scan(&user.Username, &user.Password, &user.Address); err != nil {
		http.Error(w, "wrong credentials", http.StatusBadRequest)
		return
	}

	log.Printf("The user is: name:%v ; password:%v ; email:%v\n", user.Username, user.Password, user.Address)

	files, err := parseFormFiles(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	cs := strings.Split(strings.TrimSuffix(r.FormValue("categories"), ","), ",")
	recipients, err := getRecipients(cs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := sendMails(user, recipients, subject, r.FormValue("body"), files); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func parseFormFiles(r *http.Request) ([]*multipart.FileHeader, error) {
	var files []*multipart.FileHeader
	err := r.ParseMultipartForm(1048576000)
	if err != nil {
		return files, err
	}
	//get a ref to the parsed multipart form
	m := r.MultipartForm
	//get the *fileheaders
	files = m.File["archivos"]
	return files, nil
}

func getRecipients(categories []string) ([]string, error) {
	recipients := []string{}
	if len(categories) == 0 {
		return recipients, errNoCategories
	}
	var id int
	for _, v := range categories {
		v = strings.ToLower(v)
		row := db.QueryRow("SELECT id FROM categories WHERE name=$1", v)
		if err := row.Scan(&id); err != nil {
			log.Printf("failed: %v\n", err)
			return recipients, err
		}
		rows, err := db.Query("SELECT address FROM emails WHERE category_id=$1", id)
		if err != nil {
			log.Printf("failed to retrieve addresses: %v\n", err)
			return recipients, err
		}
		defer rows.Close()
		var address string
		for rows.Next() {
			if err := rows.Scan(&address); err != nil {
				log.Printf("failed: %v\n", err)
				return recipients, err
			}
			recipients = append(recipients, address)
		}
	}
	return recipients, nil
}

func readFile(v *multipart.FileHeader) ([]byte, error) {
	var result []byte
	f, err := v.Open()
	if err != nil {
		return result, err
	}
	buf := make([]byte, 100)
	for {
		n, err := f.Read(buf[0:])
		result = append(result, buf[0:n]...)
		if err != nil {
			if err == io.EOF {
				break
			}
			return result, err
		}
	}
	return result, nil
}

func sendMails(user User, recipients []string, subject, body string, files []*multipart.FileHeader) error {
	d := gomail.NewDialer("smtp.antoniopan.com.ar", 25, user.Username, user.Password)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	s, err := d.Dial()
	if err != nil {
		return err
	}
	defer s.Close()

	m := gomail.NewMessage()
	for _, v := range files {
		result, err := readFile(v)
		if err != nil {
			return err
		}
		m.Attach(v.Filename, gomail.SetCopyFunc(func(w io.Writer) error {
			_, err = w.Write(result)
			return err
		}))
	}
	for _, v := range recipients {
		m.SetHeader("From", user.Address)
		m.SetHeader("Subject", subject)
		m.SetBody("text/html", body)
		v = strings.TrimSpace(v)
		log.Printf("Sending to: %v\n", v)
		m.SetAddressHeader("To", v, v)
		if err := gomail.Send(s, m); err != nil {
			log.Printf("Could not send email to %q: %v", v, err)
		}
		m.Reset()
		log.Printf("Done sending to: %v\n", v)
	}
	return nil
}

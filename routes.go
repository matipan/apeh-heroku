package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

// Route represents a route with a path, method, handler and authentication
type Route struct {
	Path        string
	Method      string
	Auth        bool
	HandlerFunc http.HandlerFunc
}

// Routes is a set of routes
type Routes []Route

// NewRouter create a new mux router and registers all routes
func NewRouter() *mux.Router {
	router := mux.NewRouter()
	for _, route := range routes {
		if !route.Auth {
			router.Methods(route.Method).
				Path(route.Path).
				HandlerFunc(route.HandlerFunc)
		} else {
			router.Methods(route.Method).
				Path(route.Path).
				HandlerFunc(authenticate(route.HandlerFunc))
		}
	}

	return router
}

var routes = Routes{
	Route{
		"/",
		"GET",
		true,
		homepageHandler,
	},
	Route{
		"/emails/{category}",
		"GET",
		true,
		homepageHandler,
	},
	Route{
		"/mails/send",
		"POST",
		true,
		mailerHandler,
	},
	Route{
		"/category/new",
		"POST",
		true,
		newCategoryHandler,
	},
	Route{
		"/category/email/new",
		"POST",
		true,
		newEmailHandler,
	},
	Route{
		"/login",
		"GET",
		false,
		loginPage,
	},
	Route{
		"/login/check",
		"POST",
		false,
		loginHandler,
	},
	Route{
		"/user/new",
		"POST",
		true,
		newUserHandler,
	},
}

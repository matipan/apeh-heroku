package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq"
)

var db *sql.DB

const (
	CreateCategories = `
	CREATE TABLE IF NOT EXISTS categories (
		id SERIAL PRIMARY KEY,
		name VARCHAR(100) NOT NULL UNIQUE
	);
	`
	CreateEmails = `
	CREATE TABLE IF NOT EXISTS emails (
		id SERIAL PRIMARY KEY,
		category_id INTEGER NOT NULL,
		address VARCHAR(100) NOT NULL,
		UNIQUE (category_id, address)
	);
	`
	CreateUsers = `
	CREATE TABLE IF NOT EXISTS users (
		id SERIAL PRIMARY KEY,
		username VARCHAR(100) NOT NULL,
		password VARCHAR(100) NOT NULL,
		address VARCHAR(100) NOT NULL,
		UNIQUE (username, address)
	);
	`
)

func main() {
	var err error
	// for production only:
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	// for development:
	// db, err = sql.Open("postgres", "user=Matias dbname=apeh-heroku sslmode=disable")
	if err != nil {
		log.Fatalf("Error opening database: %q", err)
	}

	if _, err := db.Exec(CreateCategories); err != nil {
		log.Printf("failed to create categories table: %v\n", err)
	}
	if _, err := db.Exec(CreateEmails); err != nil {
		log.Printf("failed to create emails table: %v\n", err)
	}
	if _, err := db.Exec(CreateUsers); err != nil {
		log.Printf("failed to create users table: %v\n", err)
	}

	r := NewRouter()
	s := http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets/")))
	r.PathPrefix("/assets/").Handler(s)

	http.Handle("/", r)

	http.ListenAndServe(":"+os.Getenv("PORT"), r)
}

package main

import (
	"io/ioutil"
	"net/http"
	"time"
)

func loginPage(w http.ResponseWriter, r *http.Request) {
	c, err := ioutil.ReadFile("templates/login.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html")
	w.Write(c)
	return
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	username, password := r.FormValue("username"), r.FormValue("password")
	if username != "pablopan" && password != "contrasenia" {
		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusTemporaryRedirect)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:    "auth",
		Value:   username,
		Path:    "/",
		Expires: time.Now().Add(time.Hour * 24 * 365),
	})
	w.Header().Set("Location", "/")
	w.WriteHeader(http.StatusFound)
}

func authenticate(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := r.Cookie("auth")
		if err != nil {
			w.Header().Set("Location", "/login")
			w.WriteHeader(http.StatusTemporaryRedirect)
		}

		next(w, r)
	})
}
